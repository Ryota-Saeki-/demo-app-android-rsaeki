package com.example.mydemoapp

import android.app.DatePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.activity_form.*
import kotlinx.android.synthetic.main.activity_form.view.*
import kotlinx.android.synthetic.main.activity_form2.*
import java.util.*

class FormActivity : AppCompatActivity() {

    private val spinnerItem = arrayListOf<String>()
    private val spinnerItem2 = arrayListOf<String>()
    private val spinnerItem3 = arrayListOf<String>()

    companion object {
        const val NAME = "com.office-fun.NAME.TEXTDATA"
        const val AGE = "com.office-fun.AGE.TEXTDATA"
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_form)
        supportActionBar?.title = "アンケート"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        val radioGroup = findViewById<RadioGroup>(R.id.gender)
        radioGroup.check(R.id.Men_Button)


        val rg_gender : RadioGroup
        val Men_Button : RadioButton
        val Women_Button : RadioButton
        val other_Button : RadioButton
        var index = ""

        rg_gender = findViewById(R.id.gender)
        Men_Button = findViewById(R.id.Men_Button)
        Women_Button = findViewById(R.id.Women_Button)
        other_Button = findViewById(R.id.other_Button)

        var item : String? = null
        var item2 : String? = null
        var item3 : String? = null

        var inter_box = ""
        var news_box  = ""
        var friend_box = ""
        var semina_box = ""
        var other_box = ""

        val checkBox1 = findViewById<CheckBox>(R.id.inter_checkBox)
        val checkBox2 = findViewById<CheckBox>(R.id.news_checkBox)
        val checkBox3 = findViewById<CheckBox>(R.id.friend_checkBox)
        val checkBox4 = findViewById<CheckBox>(R.id.semi_checkBox)
        val checkBox5 = findViewById<CheckBox>(R.id.other_checkBox)




        //Spinerの処理
        //YEAR
        for(i in 1930..2020){
            spinnerItem.add(i.toString())
        }
        val yspinner = findViewById<Spinner>(R.id.year_spinner)
        val adapter = ArrayAdapter(applicationContext,android.R.layout.simple_spinner_item,spinnerItem)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        yspinner.adapter = adapter
        yspinner.onItemSelectedListener = object:AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val spinnerParent = parent as Spinner
                item = spinnerParent.selectedItem as String
            }
        }

        //Month
        for(i in 1..12){
            spinnerItem2.add(i.toString())
        }
        val mspinner = findViewById<Spinner>(R.id.mon_spinner)
        val adapter2 = ArrayAdapter(applicationContext,android.R.layout.simple_spinner_item,spinnerItem2)
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        mspinner.adapter = adapter2
        mspinner.onItemSelectedListener = object:AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val spinnerParent = parent as Spinner
                item2 = spinnerParent.selectedItem as String
            }
        }

        //Day
        for(i in 1..31){
            spinnerItem3.add(i.toString())
        }
        val dspinner = findViewById<Spinner>(R.id.day_spinner)
        val adapter3 = ArrayAdapter(applicationContext,android.R.layout.simple_spinner_item,spinnerItem3)
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        dspinner.adapter = adapter3
        dspinner.onItemSelectedListener = object:AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val spinnerParent = parent as Spinner
                item3 = spinnerParent.selectedItem as String
            }
        }

        //送信ボタンが押された時
        send_button.setOnClickListener {
            val intent = Intent(this, Form2Activity::class.java)

            if(rg_gender.checkedRadioButtonId != -1){
                if(Men_Button.isChecked)
                    index = "男性"
                else if (Women_Button.isChecked)
                    index = "女性"
                else if (other_Button.isChecked)
                    index = "その他"
            }

            if(checkBox1.isChecked)
                inter_box = "インターネット"
            else
                inter_box = ""

            if(checkBox2.isChecked)
                news_box = "雑誌記事"
            else
                news_box = ""

            if(checkBox3.isChecked)
                friend_box = "友人・知人から"
            else
                friend_box = ""

            if(checkBox4.isChecked)
                semina_box = "セミナー"
            else
                semina_box = ""

            if(checkBox5.isChecked)
                other_box = "その他"
            else
                other_box = ""


            intent.putExtra(NAME, NameText.text.toString())
            intent.putExtra(AGE,ageText.text.toString())
            intent.putExtra("gender",index)
            intent.putExtra("YEAR",item)
            intent.putExtra("MON",item2)
            intent.putExtra("DAY",item3)
            intent.putExtra("INTERNET",inter_box)
            intent.putExtra("NEWS",news_box)
            intent.putExtra("FRIEND",friend_box)
            intent.putExtra("SEMINA",semina_box)
            intent.putExtra("OTHER",other_box)

            startActivity(intent)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}
