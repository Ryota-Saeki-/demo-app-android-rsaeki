package com.example.mydemoapp



import android.net.Uri
import android.os.Bundle
import android.webkit.JsPromptResult
import android.webkit.ValueCallback
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
class WebViewActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        supportActionBar?.title = "WebView"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)
        val webView = findViewById(R.id.webview) as WebView
        webView.loadUrl("https://www.sonix.asia/")
    }
    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}
//import androidx.appcompat.app.AppCompatActivity
//import android.os.Bundle
//import android.webkit.WebView
//
//class WebViewActivity: AppCompatActivity() {
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        supportActionBar?.title="Web View"
//        supportActionBar?.setDisplayHomeAsUpEnabled(true)
//        setContentView(R.layout.activity_web_view)
//
//        val webView = findViewById(R.id.webview) as WebView
//        webView.loadUrl("https://www.sonix.asia/")
//
//    }
//
//    override fun onSupportNavigateUp(): Boolean {
//        finish()
//        return super.onSupportNavigateUp()
//    }
//}
