package com.example.mydemoapp

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.SeekBar
import androidx.core.graphics.ColorUtils
import kotlinx.android.synthetic.main.activity_picker.*

class PickerActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.title = "Slider"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setContentView(R.layout.activity_picker)

        sbR.max = 255
        sbR.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                val colorStr = getColorString()
//                strColor.setText(colorStr.replace("#", "").toUpperCase())
                colorview.setBackgroundColor(Color.parseColor(colorStr))
            }
            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })
        sbG.max = 255
        sbG.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                val colorStr = getColorString()
//                strColor.setText(colorStr.replace("#", "").toUpperCase())
                colorview.setBackgroundColor(Color.parseColor(colorStr))
            }
            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })
        sbB.max = 255
        sbB.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                val colorStr = getColorString()
//                strColor.setText(colorStr.replace("#", "").toUpperCase())
                colorview.setBackgroundColor(Color.parseColor(colorStr))
            }
            override fun onStartTrackingTouch(seekBar: SeekBar) {}
            override fun onStopTrackingTouch(seekBar: SeekBar) {}
        })
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
    fun getColorString(): String {
//        var a = Integer.toHexString(((255*sbR.progress)/sbR.max))
//        if(a.length==1) a = "0"+a
        var r = Integer.toHexString(((255*sbR.progress)/sbR.max))
        if(r.length==1) r = "0"+r
        var g = Integer.toHexString(((255*sbG.progress)/sbG.max))
        if(g.length==1) g = "0"+g
        var b = Integer.toHexString(((255*sbB.progress)/sbB.max))
        if(b.length==1) b = "0"+b
        return "#" + r + g + b
    }


}

