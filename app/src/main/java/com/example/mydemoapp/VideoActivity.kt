package com.example.mydemoapp

import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.webkit.WebChromeClient
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.MediaController
import kotlinx.android.synthetic.main.activity_video.*
import kotlinx.android.synthetic.main.activity_web_view.*

class VideoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.title = "Video"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        setContentView(R.layout.activity_video)


        imageButton.setOnClickListener {
            Handler(mainLooper).postDelayed({
                var moviePath = Uri.parse("android.resource://" + packageName + "/" + R.raw.glass)
                videoView.setVideoURI(moviePath)
                //seyVideoXXXメゾットは非同期なので、リスナーを設定し、準備が完了してから再生するように実装する
                videoView.setOnPreparedListener {
                    videoView.start()
                    //再生メニューの表示
                    videoView.setMediaController(MediaController(this))
                }
            }, 200)
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)

        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            supportActionBar?.hide()
        } else {
            supportActionBar?.show()
        }
    }


    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
}
